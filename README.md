# TaskRuby

## Задание

employee  = Company::Employee.new("Egor Nikolayev")

statement = Company::VacationStatement.new(employee, '12.04.2022', '21.04.2022')

hr_dept   = Company::HrDept.new

hr_dept.accept_statement(vacation_statement) #hr_dept хранит в себе все поданые заявления"

=> "измените длительность - не более 15 дней"

=> "измените интервал - пересекается с другими работниками"

=> либо true либо рэйзим ошибку с пояснением
